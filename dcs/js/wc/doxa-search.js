import {data} from './doxa-search-data.js'
import {labels} from './doxa-search-labels.js'

class DoxaSearch extends HTMLElement {
  constructor() {
    super();
    this.availableLangs = [];
    this.selectedLang = 0;
    this.curPage = 1;
    this.currentIdFilter = "";
    this.currentValueFilter = "";
    this.dataIndexArray = [];
    this.dataLength = 0;
    this.dataLength = 1;
    this.pageSize = 9; // should be a multiple of 3
    this.scrollY = "";
    this.theData = {};
    this.originalValue = "";
    this.isBool = false;
    this.isNumber = false;
    this.divMatches = null;
    this.selectAvailableLangs = null;
    this.inputFilter = null;
    this.wordBoundary = `[ |\.|;|?|!|"|'|:|˙|$|^|\(|\)]`;
    this.target = "_self"
  }

  connectedCallback() {
    this.innerHTML =
        `
<style> 
  @import url('../../css/bootstrap.min.css');
  @import url('../../img/bootstrap-icons.css');
</style>
<style>
    body {
      background-color: whitesmoke !important;
    }
    .card {
      background-color: white;
    }
    .search {
        margin-left: 20px;
        margin-right: 20px;
        background-color: whitesmoke;
    }
    .search-page-navigation-top, search-page-navigation-bottom, #currentPageTop, #pageSizeIdTop,  #currentPageBottom, #pageSizeIdBottom, .page-link {
      background-color: whitesmoke;
    }
    .search-page-navigation-top {
        margin-top: 0;
        margin-bottom: 0;
    }
    .search-page-navigation-bottom {
        padding: .25rem !important;
    }
    .alert {
      line-height: 1px;
    }
    .filter: {
    background-color: whitesmoke !important;
    }
    .filterColor {
      color: blue;
    }
    .page-title {
        color: blue;
        font-style: italic;
    }
    .link {
        margin-left: 15px;
    }
    .links {
        margin-left: 15px;
    }
    .matches {
        flex-direction: row;
        flex-wrap: wrap;
        padding-left: 20px;
        padding-right: 20px;
        padding-bottom: 20px;
    }
    .search-options {
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
    }
    .search-options > * {
        justify-content: flex-start;
        flex-basis: 50%; 
        min-width: 50%;
        max-width: 50%;
    }
    #availableLangs {
        flex-basis: fit-content !important;
        margin-bottom: 20px;
    }
    .filter-options {
        flex-direction: row;
    }
    @media (max-width: 480px) {
      .search-options {
        display: block;
      }
      .search-options > * {
        flex-basis: 100%; 
        min-width: 100%;
      }
    }
</style>
<div class="search">
<h3 class="text-center page-title">${labels.TitlePage}</h3>
<!-- Help -->
    <div class="row p-1">
      <div class="container-fluid" >
        <help-info id="notAvailable" class="d-none" text="${labels.NotAvailable}."></help-info>
        <help-info id="available" text="${labels.Desc}"></help-info>
      </div>
    </div>
<!-- Filter -->
<div class="Filter">    
<!-- Selector -->
  <div id="divSearchOption" class="search-options">
    <select id="availableLangs" class="form-select" aria-label="Default select example"></select>
    <div class="filter-options">
      <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">
            <i class="bi-filter" style="color: cornflowerblue;"></i>
        </span>
        <input type="text" id="search-filter-value" class="form-control w-50" placeholder="${labels.PlaceHolder}" aria-label="filter value" aria-describedby="basic-addon1">
      </div>
      <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="search-option-exact" value="option1">
          <label class="form-check-label" for="search-option-exact">${labels.Exact}</label>
      </div>
      <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="search-option-whole-word"
                 value="option2">
          <label class="form-check-label" for="search-option-whole-word">${labels.WholeWord}</label>
      </div>
    </div>
  </div>
    <div class="row p-1">
      <div class="container-fluid" >
            <div class="col">
                <div class="alert alert-primary" id="app-settings-alert" role="alert">
                    <span>${labels.Status}:</span>
                </div>
            </div>
            <div class="col">
            </div>
            <div class="col">
            </div>
      </div>
    </div>
    </div>
    <!-- Top Pagination -->
     <div class="row search-page-navigation-top d-none align-middle">
        <div class="col col-4">
            <div class="control-label">${labels.ShowingMatches} <span id="fromTop">0</span> ${labels.To} <span id="toTop">0</span> ${labels.Of} <span id="countTop">0</span></div>
        </div>
        <div class="col col-4"><div class="control-label" style="text-align: center;"><span>${labels.PageSize}: </span><input id="pageSizeIdTop" style="max-width: 3ch; text-align: center;"></div></div>
        <div class="col col-4">
            <nav aria-label="Page navigation">
            <ul class="pagination justify-content-end">
              <li class="page-item">
                <a id="firstButtonTop" class="page-link d-none" href="#" aria-label="First">
                  <span aria-hidden="true"><i class="bi-chevron-double-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="prevButtonTop" class="page-link d-none" href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="bi-chevron-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <div class="control-label" style=""> 
                  <span style="margin-left: 4px;">${labels.Page}</span>
                  <input style="margin-left: 2px;max-width: 4ch; text-align: center;" id="currentPageTop">
                  <span style="margin-left: 2px;">${labels.Of}</span>
                  <span style="margin-left: 2px;margin-right: 4px;" id="pageMaxTop"></span>
                </div>
              </li>
              <li class="page-item">
                <a id="nextButtonTop" class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="bi-chevron-right"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="lastButtonTop" class="page-link" href="#" aria-label="Last">
                  <span aria-hidden="true"><i class="bi-chevron-double-right"></i></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        </div>

<!-- Matches Div -->
    <div class="matches flex">
      <div id="divMatches" class="row row-cols-1 row-cols-md-3 g-4">
      </div>
    </div>
<!-- Bottom Pagination -->
     <div class="row search-page-navigation-bottom d-none align-middle">
        <div class="col col-4">
            <div class="control-label" style="">${labels.ShowingMatches} <span id="fromBottom">0</span> ${labels.To} <span id="toBottom">0</span> ${labels.Of} <span id="countBottom">0</span></div>
        </div>
        <div class="col col-4"><div class="control-label" style="text-align: center;"><span>${labels.PageSize}: </span><input id="pageSizeIdBottom" style="max-width: 3ch; text-align: center;"></div></div>
        <div class="col col-4">
            <nav aria-label="Page navigation">
            <ul class="pagination justify-content-end">
              <li class="page-item">
                <a id="firstButtonBottom" class="page-link d-none" href="#" aria-label="First">
                  <span aria-hidden="true"><i class="bi-chevron-double-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="prevButtonBottom" class="page-link d-none" href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="bi-chevron-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <div class="control-label" style=""> 
                  <span style="margin-left: 4px;">${labels.Page}</span>
                  <input style="margin-left: 2px;max-width: 4ch; text-align: center;" id="currentPageBottom">
                  <span style="margin-left: 2px;">${labels.Of}</span>
                  <span style="margin-left: 2px;margin-right: 4px;" id="pageMaxBottom"></span>
                </div>
              </li>
              <li class="page-item">
                <a id="nextButtonBottom" class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="bi-chevron-right"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="lastButtonBottom" class="page-link" href="#" aria-label="Last">
                  <span aria-hidden="true"><i class="bi-chevron-double-right"></i></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        </div>
     </div>
  </div>
</div>

`;

    this.inputFilter = this.querySelector("#search-filter-value");
    this.divMatches = this.querySelector("#divMatches");
    this.selectAvailableLangs = this.querySelector("#availableLangs");


    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.selectAvailableLangs.addEventListener('change', (event) => this.setLangSelection(event));
    this.inputFilter.addEventListener('keyup', (event) => this.filterData(event));
    this.querySelector('#currentPageTop').addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.curPage = document.getElementById("currentPageTop").value;
        document.getElementById("currentPageBottom").value = this.curPage;
        this.renderMatches();
      }
    });
    this.querySelector('#currentPageBottom').addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.curPage = document.getElementById("currentPageBottom").value;
        document.getElementById("currentPageTop").value = this.curPage;
        this.renderMatches();
      }
    });
    // listen for page size change
    this.querySelector("#pageSizeIdTop").addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.pageSize = this.querySelector('#pageSizeIdTop').value;
        this.querySelector('#pageSizeIdBottom').value = this.pageSize;
        this.curPage = 1;
        this.renderMatches();
      }
    });
    this.querySelector("#pageSizeIdBottom").addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.pageSize = this.querySelector('#pageSizeIdBottom').value;
        this.querySelector('#pageSizeIdTop').value = this.pageSize;
        this.curPage = 1;
        this.renderMatches();
      }
    });

    this.querySelector('#firstButtonTop').addEventListener('click', () => this.firstPage(this));
    this.querySelector('#firstButtonBottom').addEventListener('click', () => this.firstPage(this));
    this.querySelector('#prevButtonTop').addEventListener('click', () => this.previousPage(this));
    this.querySelector('#prevButtonBottom').addEventListener('click', () => this.previousPage(this));
    this.querySelector('#nextButtonTop').addEventListener('click', () => this.nextPage(this));
    this.querySelector('#lastButtonTop').addEventListener('click', () => this.lastPage(this));
    this.querySelector('#nextButtonBottom').addEventListener('click', () => this.nextPage(this));
    this.querySelector('#lastButtonBottom').addEventListener('click', () => this.lastPage(this));

    this.availableLangs = data.Langs;
    this.theData = data;
    this.setLangsDropdown();
    this.clearMatches();
    this.renderMatches();
  }

  disconnectedCallback() {
    this.selectAvailableLangs.removeEventListener('change', (event) => this.setLangSelection(event));
    this.inputFilter.removeEventListener('keyup', (event) => this.filterData(event));
    this.querySelector('#firstButtonTop').removeEventListener('click', () => this.firstPage(this));
    this.querySelector('#firstButtonBottom').removeEventListener('click', () => this.firstPage(this));
    this.querySelector('#prevButtonTop').removeEventListener('click', () => this.previousPage(this));
    this.querySelector('#prevButtonBottom').removeEventListener('click', () => this.previousPage(this));
    this.querySelector('#nextButtonTop').removeEventListener('click', () => this.nextPage(this));
    this.querySelector('#lastButtonTop').removeEventListener('click', () => this.lastPage(this));
    this.querySelector('#nextButtonBottom').removeEventListener('click', () => this.nextPage(this));
    this.querySelector('#lastButtonBottom').removeEventListener('click', () => this.lastPage(this));
  }
  clearMatches() {
    this.inputFilter.value = "";
    this.currentValueFilter = this.inputFilter.value;
    this.dataIndexArray = [];
    this.divMatches.innerHTML = "";
  }
  setLangSelection(event) {
    this.selectedLang = parseInt(event.target.value);
    this.clearMatches();
  }
  setLangsDropdown() {
    if (this.availableLangs && this.availableLangs[0].Name === "") { // in the unlikely event there is no html to search, remove the search options
      this.querySelector("#notAvailable").classList.remove("d-none");
      this.querySelector("#available").classList.add("d-none");
      this.querySelector("#divSearchOption").classList.add("d-none");
      return
    }
    let html = ``
    this.availableLangs.forEach((lang, index) => {
      if (index === 0) {
        html += `<option value="${lang.Index}" selected>${lang.Name}</option>`
        this.selectedLang = lang.Index;
      } else {
        html += `<option value="${lang.Index}">${lang.Name}</option>`
      }
    });
    this.querySelector("#availableLangs").innerHTML = html;
  }

  static get observedAttributes() {
    return [];
  }

  attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }

  getValueFilterRegEx() {
    let pattern = this.currentValueFilter;
    let modifier = "";
    if (this.searchWithWholeWord()) {
      // \b word boundary only works with ASCII, not languages like Greek
      if (pattern.startsWith("\\b")) {
        pattern = pattern.replaceAll("\\b", "");
      }
      pattern = this.wordBoundary + pattern + this.wordBoundary;
    }
    pattern = pattern.replace("(?i)", "");
    if (!this.searchExact()) {
      modifier = "i";
    }
    modifier += "gu";
    return new RegExp(pattern, modifier);
  }
  searchExact() {
    return this.querySelector('#search-option-exact').checked;
  }

  searchWithWholeWord() {
    return this.querySelector('#search-option-whole-word').checked;
  }

  setPatternOptions() {
    const searchInput = this.querySelector('#search-input');
    let pattern = searchInput.value;
    if (pattern.length === 0) {
      return;
    }
    const optionExact = this.querySelector('#search-option-exact');
    if (optionExact) {
      this.currentSearch = pattern;
    } else {
      this.currentSearch = utils.toNormalForm(pattern);
    }
    searchInput.value = this.currentSearch;
  }

  renderMatches() {
    this.divMatches.innerHTML = "";
    this.dataLength = this.dataIndexArray.length;
    if (this.dataLength === 0) {
      this.setAlert(`0 ${labels.FoundSingular}`);
      this.setTableSizeNotice(1, 0);
      return;
    }
    let displaying = 0;
    let start = (this.curPage - 1) * this.pageSize;
    let end = this.curPage * this.pageSize;

    // set up filter regEx to highlight matching filter words in value column only
    let filterRegEx = {};
    if (this.currentValueFilter) {
      filterRegEx = this.getValueFilterRegEx();
    }

    // add cards
    this.dataIndexArray.filter((row, index) => {
      if (index >= start && index < end) return true;
    }).forEach((item, index) => {
      displaying++;
      const dataItem = this.theData.Docs[this.selectedLang][item];
      const divCol = document.createElement("div")
      divCol.classList.add("col")
      // highlight the matching text
      let searchString = dataItem.Entry.text;
      if (this.currentValueFilter) {
        searchString = searchString.replace(filterRegEx, (match) => {
          return `<span class='filterColor'>${match}</span>`;
        });
      }
      let html =
          `
        <div class="card h-100">
            <div class="card-body">
              <p class="card-text">${searchString}<span class="versiondesignation">[${dataItem.Entry.version}]</span></p>
              <hr>`
      const linkMap = this.getLinkMap(dataItem.Links);
      for (const [key, links] of linkMap) {
        html = html + `<p class="links index-service-day align-top">${key}<span>`
        for (const [lKey, link] of links) {
          html = html + `<a class="link index-file-link" href="${link.href}#${link.row}" target="${this.target}">${link.desc} (${link.rows.length+1}x)</a>`
        }
      }
      html = html + `</span></p></div>`
      html = html + `            
            <div class="card-footer">
              <p><span class="source">${labels.Source}: </span><span class="index-file-link">${dataItem.Entry.desc}</span></h5>
              <p><span class="source">${labels.DbId}: </span><a class="index-file-link" href="${dataItem.Entry.tkHref}" target="${this.target}">${dataItem.Entry.id}</a></p>
            </div>
`
      divCol.innerHTML = html;
      this.divMatches.appendChild(divCol);
    });
    let found = labels.FoundPlural;
    if (this.dataLength === 0) {
      found = labels.FoundSingular;
    }
    this.setAlertInfo(this.dataLength.toLocaleString() + " " + found);
    this.setTableSizeNotice(start, displaying);
  }
  setAlertInfo(msg) {
    this.querySelector("#app-settings-alert").className = "alert alert-primary";
    this.setAlert(msg);
  }

  getLinkMap(links) {
    const masterMap = new Map();
    links.forEach((l) => {
      let parts = l.desc.split(" (");
      let key = "";
      let desc = "";
      if (parts.length == 2) {
        key = parts[0];
        desc = parts[1].slice(0,parts[1].length-1);
      }
      let linkMap = new Map();
      if (masterMap.has(key)) {
        linkMap = masterMap.get(key);
      }
      let linkInfo = {};
      if (linkMap.has(l.href)) {
        linkInfo = linkMap.get(l.href);
        linkInfo.rows.push(l.row)
      } else {
        linkInfo.desc = desc;
        linkInfo.href = l.href
        linkInfo.row = l.row;
        linkInfo.rows = [];
      }
      linkMap.set(l.href, linkInfo);
      masterMap.set(key, linkMap);
    });
    return masterMap;
  }

  setAlert(msg) {
    this.querySelector("#app-settings-alert").innerText = `${labels.Status}: ` + msg;
  }
  // Populates theDataIndexArray with indexes of this.theData.
  // If pattern is null, all the indexes of this.theData will be pushed onto the array.
  // If pattern is not null, only indexes of data IDs or Values that match will be pushed onto the array.
  filterData(evt) {
    // ignore keypress unless user presses enter key
    if (evt.keyCode !== 13) {
      return;
    }
    this.currentValueFilter = this.inputFilter.value;
    this.dataIndexArray = [];
    if (this.currentValueFilter.length > 0) {
      const valueRegEx = this.getValueFilterRegEx();
      this.theData.Docs[this.selectedLang].forEach((item, index) => {
        if (valueRegEx.test(item.Entry.text)) {
          this.dataIndexArray.push(index);
        }
      });
    }
    this.curPage = 1;
    this.renderMatches();
  }

  setTableSizeNotice(start, displaying) {
    const end = start + displaying
    start = start + 1
    this.querySelector(".search-page-navigation-top").classList.add("d-none");
    this.querySelector(".search-page-navigation-bottom").classList.add("d-none");
    this.querySelector("#fromTop").innerText = `${start.toLocaleString()}`;
    this.querySelector("#toTop").innerText = `${end.toLocaleString()}`;
    this.querySelector("#countTop").innerText = `${this.dataLength.toLocaleString()}`;
    this.querySelector("#fromBottom").innerText = `${start.toLocaleString()}`;
    this.querySelector("#toBottom").innerText = `${end.toLocaleString()}`;
    this.querySelector("#countBottom").innerText = `${this.dataLength.toLocaleString()}`;
    this.querySelector("#currentPageTop").value = `${this.curPage.toLocaleString()}`;
    this.querySelector("#currentPageBottom").value = `${this.curPage.toLocaleString()}`;
    this.querySelector("#pageMaxTop").innerText = `${(Math.ceil(this.dataLength / this.pageSize)).toLocaleString()}`;
    this.querySelector("#pageMaxBottom").innerText = `${(Math.ceil(this.dataLength / this.pageSize)).toLocaleString()}`;
    this.querySelector("#pageSizeIdTop").value = `${this.pageSize}`;
    this.querySelector("#pageSizeIdBottom").value = `${this.pageSize}`;

    if (this.curPage === 1) {
      this.querySelector("#firstButtonTop").classList.add("d-none");
      this.querySelector("#firstButtonBottom").classList.add("d-none");
      this.querySelector("#prevButtonTop").classList.add("d-none");
      this.querySelector("#prevButtonBottom").classList.add("d-none");
    } else {
      this.querySelector("#firstButtonTop").classList.remove("d-none");
      this.querySelector("#firstButtonBottom").classList.remove("d-none");
      this.querySelector("#prevButtonTop").classList.remove("d-none");
      this.querySelector("#prevButtonBottom").classList.remove("d-none");
    }
    if (this.curPage === Math.ceil(this.dataLength / this.pageSize)) {
      this.querySelector("#nextButtonTop").classList.add("d-none");
      this.querySelector("#lastButtonTop").classList.add("d-none");
      this.querySelector("#nextButtonBottom").classList.add("d-none");
      this.querySelector("#lastButtonBottom").classList.add("d-none");
    } else {
      this.querySelector("#nextButtonTop").classList.remove("d-none");
      this.querySelector("#lastButtonTop").classList.remove("d-none");
      this.querySelector("#nextButtonBottom").classList.remove("d-none");
      this.querySelector("#lastButtonBottom").classList.remove("d-none");
    }
    if (displaying < this.dataIndexArray.length) {
      this.querySelector(".search-page-navigation-top").classList.remove("d-none");
      this.querySelector(".search-page-navigation-bottom").classList.remove("d-none");
    }
  }
  scrollWindowTo(y) {
    window.scrollTo({top: y, behavior: 'auto'});
  }

  firstPage() {
    if (this.curPage > 1) this.curPage = 1;
    this.renderMatches();
  }

  previousPage() {
    if (this.curPage > 1) this.curPage--;
    this.renderMatches();
  }

  nextPage() {
    if ((this.curPage * this.pageSize) < this.dataLength) this.curPage++;
    this.renderMatches();
  }

  lastPage() {
    if ((this.curPage * this.pageSize) < this.dataLength) {
      this.curPage = Math.floor(this.dataLength / this.pageSize) + 1;
      this.renderMatches();
    }
  }

  resetTableSizeNotice() {
    document.getElementById("fromTop").innerText = "0";
    document.getElementById("toTop").innerText = "0";
    document.getElementById("countTop").innerText = "0";
    document.getElementById("fromBottom").innerText = "0";
    document.getElementById("toBottom").innerText = "0";
    document.getElementById("countBottom").innerText = "0";
    document.getElementById("currentPageTop").value = "0";
    document.getElementById("currentPageBottom").value = "0";
    document.getElementById("pageMaxTop").innerText = "0";
    document.getElementById("pageMaxBottom").innerText = "0";
    document.getElementById("pageSizeIdTop").value = "0";
    document.getElementById("pageSizeIdBottom").value = "0";

  }
}

window.customElements.define('doxa-search', DoxaSearch);